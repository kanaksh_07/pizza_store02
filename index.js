const express = require("express");
const app = express();
const path = require("path");
const { engine } = require("express-handlebars");
const PORT = 3000;


const PizzaRoutes = require("./app/routes/PizzaRoutes");

app.use(express.static(__dirname + '/public'));



app.set("views", path.join(__dirname, "app/views"));
app.engine(
    "handlebars",
    engine({
        defaultLayout: "index",
        runtimeOptions: {
            allowProtoPropertiesByDefault: true,
            allowProtoMethodsByDefault: true
        },
         partialsDir: __dirname + "/app/views/layouts",
        // helpers: require("./app/helper/LogicHelper"),
        // library: require('./app/library/GeneralLibrary'),
    })
);
app.set("view engine", "handlebars");



app.use("/", PizzaRoutes);



console.log(" branch kanak");
app.listen(PORT, () => {
    console.log(`http://localhost:${PORT}`);
})










