const express = require("express");
const router = express.Router();
const PizzaController = require("../controllers/PizzaController");

router.get("/",PizzaController.index);

module.exports = router;